Rails.application.routes.draw do
  resources :lessons do 
  	get 'delete'
  end 
  root 'lessons#edit', id: 1
  resources :comments, only: [:create, :destroy]
  
end
