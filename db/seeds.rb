# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Lesson.delete_all 
Lesson.create!([

	{
		name: 'Chào người anh em!',
		instruction: '**Thẻ HTML** là để bạn đánh dấu văn bản mình gõ. Ví dụ bạn bọc chữ "xin chào" trong thẻ ```<b> xin chào </b>``` *(b là bold - đậm, mạnh)*, thì khi bạn đưa văn bản này cho trình duyệt, hắn sẽ hiểu được là bạn muốn thể hiện chữ "xin chào" một cách mạnh mẽ, vì thế hắn lụi cụi đi in đậm. HTML, mặc dù có rất nhiều thẻ, ==chỉ toàn là thẻ mà thôi==. Hiểu được vậy là hiểu được HTML.

**Các thẻ căn bản**

 -  ```<h1> </h1>``` là tiêu đề 1 (header 1), tức là tiêu đề to nhất. Html có từ thẻ h1 đến thẻ h6.

 -  ```<p> </p>``` là đoạn văn (paragraph) chuyên để bọc quanh các đoạn văn.

**_Bài tập_** Trong khung bên trái, sửa cụm *Chào thân ái* thành *Tạm biệt* để thấy thay đổi tự động trong khung bên phải.

- Sửa tên Minh thành tên bạn hoặc bất kỳ.',
		body: '<h1>Chào thân ái, đồng chí nhỏ!</h1>
 <p> Tên tôi là Minh. </p>
 <p> Tôi 14 tuổi. </p>',
		note: '**Bài tập thêm** Làm trang web dưới máy của bạn:

- Mở phần mềm viết văn bản thuần trên máy bạn (gedit hoặc notepad) 

- Bôi đen toàn bộ phần trong khung làm bài tập ở trên (bên trái) (```Ctrl+A```) sau đó copy (```Ctrl+C```) và dán vào văn bản trên máy của bạn (```Ctrl+V```),

- Nếu bạn không copy được thì tự gõ đoạn code HTML trên (khung trên bên trái) vào văn bản.

- Lưu tập tin trên máy (```Ctrl+S```) với tên ```webcuatoi.html``` chả hạn, 

- Mở tập tin ra (thường là máy tính tự nhận đuôi .html và bảo trình duyệt ra mở hoặc bạn ấn chuột phải chọn mở tập tin bằng trình duyệt). Bạn sẽ thấy trình duyệt của bạn hiển thị văn bản như khung hiển thị bên phải. Có điều khác là trình duyệt của bạn sẽ dùng mặt chữ chuẩn nên không được đẹp thế kia. Mặt chữ kia là do tôi đã chọn trước, như nào sẽ nói sau.'
				},

	{
		name: 'Mưa buồn viễn xứ',
		instruction: 'Ở bài 1, bạn đã hiểu được bố cục văn bản và làm quen với các thẻ ```h1, p```, bây giờ bạn nên nghĩ về bố cục của trang web bạn định trình bày. Cũng như một bài văn, một trang web có đầu trang, phần chính chứa các đoạn, sau đó là chân trang. HTML có các thẻ tương ứng là ```header, main, section, footer``` . Tôi lấy thí dụ bạn tạo một trang web:

**Đầu trang** Bạn muốn đầu trang là phần chứa tên trang web. Do đó bạn bọc tên trang trong thẻ header:

```html
<header>
 <h1> Mưa buồn viễn xứ </h1>
 <h6> ngày buồn tháng nhớ năm thương </h6>
</header>
```

**Phần chính** Có 2 khúc:

- Khúc một có đầu đề là Hoa vàng nhớ quê mô tả nỗi buồn thống thiết của một trái tim xa xôi nửa vòng thế giới hướng về  bầu trời Hà Nội. Bạn sẽ gói cả khúc này vào thẻ section:

```html
<section>
 <h2> Hoa vàng nhớ quê </h2>
 <p> ... </p>
</section>
```
- Khúc hai

```html
<section>
 <h2> Chiều biên ải </h2>
 <p> ... </p>
</section>
```

**Chân trang** bạn muốn để tên tác giả: 

```html
<footer>
 <p> Việt Kiều yêu nước 2019 </p>
<footer>
```

**_Bài tập_**

- Sáng tác nội dung trong thẻ ```<p></p>``` của khúc 1 (section)

- Viết thêm khúc 2 (tạo thêm một thẻ ```<section> </section>``` và điền nội dung vào đó).

- Bọc cả 2 khúc (2 section) trong thẻ ```<main> </main>``` để máy tính hiểu rằng đó là phần chứa nội dung chính của trang.

- Nghĩ về sự tương tự giữa *cấu trúc văn bản* và *cấu trúc trang web*. (Trong cấu trúc văn bản ta dùng thẻ h1 cho phần đầu đề, thẻ p cho phần nhỏ. Khi nói đến cấu trúc trang web, ta dùng thẻ header cho đầu trang, thẻ section cho phần nhỏ.)

- Trong phần đầu trang, tôi dùng thẻ ```<em> </em>``` cho tiêu đề phụ, thẻ này có tác dụng gì?',
		body: '<header>
 <h1> Mưa buồn viễn xứ </h1>
 <em> ngày buồn tháng nhớ năm thương </em>
</header>

<section>
 <h2> Hoa vàng nhớ quê </h2>
 <p>  </p>
</section>',
		note: '*Các bạn thấy là khi viết code trong khung tôi có cách đầu dòng một chút cho các thẻ bé bên trong thẻ lớn, và để phân biệt các phần của trang tôi để 1 dòng trắng. HTML không quan tâm những khoảng trắng này nhưng với người viết và đọc code thì dễ nhìn hơn rất nhiều.*'
		},

	{
		name: 'Giá mà có chút ảnh',
		instruction: 'Sau bài học 2, ta thấy là trang web ta làm trông vẫn hơi đơn điệu. Tất nhiên là HTML cũng có thẻ cho bạn chèn ảnh, thẻ này trông hơi khác các thẻ khác mà bạn đã thấy một chút: ```<img src="" alt="" width=""/>``` Vừa nhìn thấy thì các bạn cũng có thể nêu ra những điểm khác sau đây:

- Thẻ này *tự đóng* (tức là không cần 2 mảnh như ```<p> </p>``` chả hạn) 

- Ngoài tên thẻ là img, còn có thêm 1 tràng các thứ nữa ở bên trong. Những thứ bên trong này gọi là *tính năng* phụ thêm (```attribute```). 

    - Ví dụ: src là source tức là nguồn chứa ảnh. Đường dẫn đến nguồn ảnh sẽ được cho vào trong ngoặc kép như này: ```src="https://pinterest.com/cute-girl.jpg"``` . Trình duyệt sẽ đi theo đường dẫn bạn chỉ để tìm ảnh. Đường dẫn có thể là link trên mạng, hoặc link nội bộ ở thư mục của trang web của bạn.

    - src là tính năng phụ thêm quan trọng nhất, vì không có nó thì trình duyệt không biết đi đâu tìm ảnh bạn nói. Trong trường hợp không tìm được (do không có dẫn hoặc dẫn hỏng), trình duyệt hiển thị trắng toát, và người ta không biết chỗ đó có ảnh gì. Hoặc mặc dù có ảnh nhưng người khiếm thị dùng máy đọc để đọc cho họ nghe sẽ không thấy được ảnh, và cũng không biết ảnh đó có nội dung gì. Chính vì thế có thêm tính năng ```alt=""```. Trong ngoặc kép sẽ điền mô tả hình ảnh. Ví dụ ```alt="cute-girl"```

    - Một tính năng nữa là độ rộng của ảnh. Bình thường có thế nào trình duyệt vô tư hiển thị thế ấy. Thường như thế ảnh rất to, vỡ đẹp. Nên có thêm tính năng độ rộng, thông số cũng cho vào ngoặc kép. Ví dụ ```width="20%"```. Bạn điền "20px" cũng được, nhưng dùng phần trăm tiện hơn rất nhiều. 20% ở đây là 20% độ rộng của hộp chứa ảnh.

**_Bài tập_** Chọn  hình ảnh minh họa cho đoạn: Hoa vàng nhớ quê. Bằng thủ pháp bình luận văn học, ta có thể suy diễn vô căn cứ rằng rất có thể là tác giả nhìn thấy mai vàng nở rộ và nhớ quê tha thiết. Vì vậy ta đi tìm ảnh phản chiếu lại nội dung cảm giác này. 

**Bước 1** Tạo thẻ: ```<img src="" width="30%">``` Để cẩn thận, tôi điền sẵn độ rộng 30% :D Lúc sau xem ảnh thấy bé quá thì sửa lại nhé.

**Bước 2** Lên <a href="https://www.pexels.com" target="_blank">pexels.com</a> trên đó toàn ảnh miễn phí tha hồ dùng. Chọn một ảnh.

**Bước 3** Ấn chuột phải vào ảnh, chọn ```Copy Image Location``` tức là copy đường link dẫn đến ảnh này
<img src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.howtogeek.com%2Fwp-content%2Fuploads%2F2018%2F05%2FCopyImageLocation.png&f=1" alt="copy" width="30%" style="display:block;margin:auto"/>

**Bước 4** Dùng ```Ctrl+V``` dán link vào giữa 2 dấu ngoặc kép của tính năng ```src=""``` trong thẻ ```<img />``` vừa tạo 

*Nếu bí thì xem hình ảnh đã được chèn ở đoạn 2*',
		body: '<header>
 <h1> Mưa buồn viễn xứ </h1>
 <em> ngày buồn tháng nhớ năm thương </em>
</header>

<main>
 <section> 
  <h2> Hoa vàng nhớ quê </h2>
  <img src="" width="30%">
  <p> Nay đi làm nails về nhớ quê day dứt. </p>
 </section>

 <section>
  <h2> Chiều biên ải </h2>
  <img src="https://images.pexels.com/photos/1366901/pexels-photo-1366901.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" width="50%">
  <p> Mênh mênh mang mang phù vân Yên Tử.. </p>
 </section>
</main>

<footer>
 <p> Việt Kiều yêu nước 2019 </p>
</footer>',
		note: '*Do số lượng thẻ ta dùng đã tăng lên, rất dễ có một thẻ bị quên dấu gạch / hoặc có thẻ quên đóng, hoặc khi viết tên thẻ src quên chữ c, vv dẫn đến vỡ trang. Khi đi tìm lỗi cần kiên nhẫn.*
<img src="https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fi.pinimg.com%2F736x%2Fe2%2Faa%2Fe0%2Fe2aae04f444e8c91bcb15e96f8717018--rubber-duck-debugging-the-duck.jpg&f=1" alt="debug" width="40%" style="display:block;margin:auto" />'
	},

	{
		name: 'Neo',
		instruction: '**Thẻ neo** Bài này sẽ giới thiệu về thẻ ```<a> </a>``` mà mọi người hay dùng để chèn đường dẫn (*link*) vào trang. Ví dụ chèn đường dẫn về trang chủ của nội bộ web này, hoặc dẫn đi trang khác vào mạng mênh mông bao la. *a* là viết tắt của *anchor* tức là neo/neo đậu. Bạn neo điểm chữ này với một vị trí nào đó trong không gian mạng (có thể là một điểm trong chính web bạn hoặc đâu đó trong không gian mạng thẳm sâu).

**Tính năng** Cũng giống như thẻ ```<img />```, thẻ anchor cần có tính năng (tính năng đó là ```href=""```) chứa nguồn để biết bạn dẫn người ta đi đâu. Ví dụ dẫn về FB của Việt Kiều yêu nước 2019 ```<a href="https://facebook.com/vietkieuyeunuoc19"> </a>```

- Bạn có thể hỏi tại sao thẻ này không tự đóng? Hoặc giữa hai mảnh thẻ kia có nội dung gì? Bạn có thể thử xem. Hoặc thử đoán xem? Nếu bạn điền chữ vào giữa hai mảnh thẻ, thì đường dẫn có mặt chữ đó như ví dụ sau: Xem thêm tại <a href="https://facebook.com/ayaderaghul" target="_blank"> Việt Kiều yêu nước 2019 </a>. Bạn thấy là mặt chữ đổi màu xanh và nếu bạn di chuột qua cụm từ đó sẽ thấy báo có link ấn vào được.

- Thay vì viết chữ, bạn có thể cho một thẻ ảnh vào giữa hai mảnh thẻ neo, khi đó nếu bạn di chuột qua bề mặt ảnh, sẽ thấy là chuột đổi thành hình trỏ và ảnh đó ấn vào được, khi ấn vào sẽ dẫn bạn đi theo đường link dưới bề mặt ảnh. Như ảnh sau:

<a href="#"><img src="https://images.pexels.com/photos/1805412/pexels-photo-1805412.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="cute-girl" width="20%" style="display:block;margin:auto" /></a>

- Nếu bạn để tính năng ```href="#"``` thì đó là đường dẫn về một điểm trong chính trang này, thành ra khi làm web hay để tạm như vậy.

- Có một tính năng hay nữa là ```target="_blank"```, khi bạn thêm vào thẻ, nếu người dùng ấn chuột vào đó thì trình duyệt sẽ mở ra một tab mới để tải trang kia thay vì tải trang mới ngay tại trận. Ví dụ bạn ấn vào link sau: <a href="https://facebook.com/ayaderaghul" target="_blank"> Việt Kiều yêu nước 2019 (mở tại tab mới) </a> và <a href="https://facebook.com/ayaderaghul"> Việt Kiều yêu nước 2019 (mở tại tab này) </a>. *Nếu bạn ấn vào link mở tại tab này thì tìm cách quay trở lại nhé, hoặc bookmark trang này đi :)*

**_Bài tập_** Nhìn vào phần chân trang (thẻ ```<footer>```), bọc cụm từ Việt Kiều yêu nước 2019 vào một thẻ neo ```<a>``` và dẫn link về trang facebook của bạn hoặc bất kỳ.
*Nếu bí thì nhìn vào thẻ neo mà tôi thêm vào ở đoạn Hoa vàng nhớ quê >> ..làm nails..*',
		body: '<header>
 <h1> Mưa buồn viễn xứ </h1>
 <em> ngày buồn tháng nhớ năm thương </em>
</header>

<main>
 <section> 
  <h2> Hoa vàng nhớ quê </h2>
  <img src="" width="20%">
  <p> Nay đi làm <a href="#">nails</a> về nhớ quê day dứt. </p>
 </section>

 <section>
  <h2> Chiều biên ải </h2>
  <img src="https://images.pexels.com/photos/1366901/pexels-photo-1366901.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" width="50%">
  <p> Mênh mênh mang mang phù vân Yên Tử.. </p>
 </section>
</main>

<footer>
 <p> Việt Kiều yêu nước 2019 </p>
</footer>',
		note: '**Bài tập thêm** Để hiểu được neo nội bộ:

- Mở một thư mục trên máy tính của bạn ra,

- Tạo 2 tập tin văn bản thuần lưu tên là ```a.html``` và ```b.html```. 

- Trong văn bản *a*, tạo một thẻ neo sang văn bản *b* như sau: ```<a href="b.html">Mở b</a>```.

- Khi mở tập tin *a* bằng trình duyệt, ta thấy có đường dẫn tên là ```Mở b```.

- Ấn vào link đó, máy tính sẽ tự biết đi tìm văn bản *b* ở cùng thư mục chứa *a*.

- Xem thêm về đường link nội bộ ở trên mạng.'
	},

	{
		name: 'Năm điều bác dạy',
		instruction: 'Chỉ qua 4 bài học ngắn mà bạn đã làm được một trang blog cá nhân rồi đó :) Bài này sẽ bàn về danh sách, có thứ tự và không có thứ tự. 

- Danh sách có thứ tự là danh sách đánh số 1, 2, 3.. hoặc a, b. c.. ý rằng các nội dung nhỏ nối tiếp nhau. Để thể hiện điều này, HTML có thẻ ```<ol></ol>``` nghĩa là *ordered list* / danh sách có thứ tự. Trong thẻ này, khi ta liệt kê các thành phần của danh sách, ta bọc mỗi thành phần trong thẻ ```<li> </li>```. Ví dụ:

```html

Năm điều bác dạy
<ol>
  <li> Yêu tổ quốc, yêu đồng bào </li>
  <li> Học tập tốt, lao động tốt </li>
  <li> </li>
</ol> 
```

- Danh sách không có thứ tự là các nội dung ngang hàng và ta không dùng số mà dùng dấu tròn hoặc gạch ngang đầu dòng. Chuyên dùng cho trường hợp này, HTML có thẻ ```<ul> </ul>```, tất nhiên các thành phần bên trong thẻ cũng phải bọc riêng trong các thẻ con ```<li> </li>```

```html

Những điều diệu vợi:
<ul>
  <li> Tinh vân mây bụi </li>
  <li> Hằng số vũ trụ </li>
  <li> Em </li>
</ul>
```

**_Bài tập_** Điền vào danh sách những việc bạn phải làm hôm nay (*lưu ý danh sách này mặc định đánh số, nhưng trang của tớ bỏ đi, khi làm HTML thuần sẽ thấy đánh số*). Sau đó điền vào danh sách những thứ cần ra chợ mua. Viết danh sách dài ra tùy ý.
',
		body: '<h1> Phấn đấu làm người 4.0 </h1>
 <h2> Việc cần làm hôm nay </h2>
  <ol>
   <ul> Học HTML </ul>
   <ul> Nói phét trên mạng xã hội </ul>
   <ul> Giác ngộ </ul>
   <ul> Tiếp tục học HTML </ul>
  </ol>

<h2> Đi chợ </h2>
 <ul>
  <li> 2 trứng </li>
  <li> 1 quả cà </li>
  <li> mang thừa đôi dép tổ ong rách đáp nhau với người bán điêu và hung hãn </li>
 </ul>',
		note: ''
	},

	{
		name: 'Trắc nghiệm vật lý',
		instruction: '**Cảnh** Ta thử ngồi nghĩ về cảnh sau: Bạn muốn làm trang web trắc nghiệm vật lý. Bạn muốn học sinh có thể chọn đáp án a b c d hoặc chọn nhiều đáp án một lúc hoặc trả lời câu hỏi theo kiểu cũ, tức là viết chút tản văn, sau đó ấn nút "Nộp bài". Thông tin sẽ được gửi đi nửa vòng trái đất để bạn ghi nhận và đánh giá. Giống như bạn ra xã điền đơn đi học vậy, thẻ HTML cho chức năng này tên là ```<form> </form>``` tức là *đơn*. Trong đơn, sẽ có các phần khác nhau dùng để thu nhận thông tin đầu vào của người dùng, ví dụ thông tin đầu vào ```<input> </input>``` (tức là *đầu vào*). Các thành phần khác là ```<select> <textarea> <button>``` tức là chọn từ danh sách thả, khung nhập tản văn, và nút bấm.

**Nghĩ sâu** Trong bài này, ta bàn trước nhất về thành phần thu nhận đầu vào ```<input>```. Thông tin đầu vào lại có nhiều kiểu, kiểu chọn một đáp án, kiểu chọn nhiều đáp án, kiểu viết chữ vào hoặc kiểu tải lên tập tin viết sẵn ở trong máy. Để phân loại các kiểu thông tin đầu nào này, HTML không tạo thêm thẻ mới, mà dùng *tính năng* (*attribute*) để lưu thông tin về loại thông tin đầu vào. (*Còn nhớ tính năng là gì không ạ? Là một phần chữ viết thêm trong thẻ.*)

**Ví dụ** kiểu chọn 1 đáp án, HTML gọi kiểu này là *radio buttion* có thể là như đài ngày xưa chỉ ấn được một nút. Ví dụ: ```<input type="radio">```. Nhìn vào thẻ này, ta thấy:

- thẻ này *tự đóng* (có 1 mảnh). Tất nhiên bạn đóng cũng được.

- *type* nghĩa là *kiểu* tức là kiểu thông tin người dùng nhập vào 

- ```type="radio"``` là tính năng của thẻ ```<input>```. Máy tính sẽ hiểu rằng thông tin đầu vào này nhận vào bằng nút radio.

Vì mỗi thẻ input này là một nút tròn tương ứng với một lựa chọn, ta cần tạo nhiều thẻ input cho các lựa chọn khác. Sau đó bọc cả lũ trong thẻ form cho gọn. Ví dụ:

```html

<form>

Câu hỏi 1: Kích thước biểu kiến của mặt trăng so với của mặt trời trên bầu trời như thế nào?
 <input type="radio"> To hơn </input>
 <input type="radio"> Bé  hơn </input>
 <input type="radio"> Bằng nhau </input> 
 <input type="radio"> Kích biểu gì cơ? </input>

</form>
```

**Tính năng khác**

- ```checked``` nghĩa là đánh dấu rồi, nếu một thẻ input có tính năng này ```<input type="radio" checked>``` thì trình duyệt sẽ hiển thị nút tròn đó được ấn vào rồi.

**Các kiểu khác của thông tin đầu vào (các type khác của input)** 

- ```type="checkbox"``` nghĩa là kiểu chọn nhiều đáp án,

- ```type="text"``` nghĩa là điền văn bản 1 dòng (ví dụ tên),

- ```type="submit"``` sẽ hiện nút bấm để bạn nộp đơn 

- ```type="reset"``` sẽ xóa hết nội dung vừa điền để điền lại

**_Bài tập_** Nhìn vào khung bên phải, đọc câu hỏi 1 và chọn đáp án đúng.

- Trả lời cả câu hỏi 2 nữa.

- Điền tên vào ô sau đó ấn nút ```Reset``` để tò mò xem nút này như nào?',
	body: '<h1> Vì tình yêu vô hạn với bầu trời.. </h1>

 <img src="https://images.pexels.com/photos/355956/pexels-photo-355956.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" width="50%" style="display:block;margin:auto"/>

<form>

 <h5> Câu hỏi 1 </h5>
 <p> Kích thước biểu kiến của mặt trăng so với của mặt trời trên bầu trời như thế nào? </p>

 <input type="radio"> To hơn </input>
 <input type="radio"> Bé  hơn </input>
 <input type="radio"> Bằng nhau </input> 
 <input type="radio"> Kích biểu gì cơ? </input>

 <h5> Câu hỏi 2 </h5>
 <p> Theo mô hình hạt, hạt nhân nguyên tử có thể chứa những loại hạt nào? </p>

 <input type="checkbox"> Electron </input>
 <input type="checkbox"> Neutron </input>
 <input type="checkbox"> Positron </input>

 <h5> Điền tên và nộp bài </h5>
 Tên: <input>
 <input type="submit" value="Nộp bài">
 <input type="reset">
</form>
',
		note: '*Bạn tinh mắt có thể thấy trong thẻ hình ảnh tôi chèn vào có thêm một tính năng mới gì thế kia? Cũng không cần hiểu ngay, phần sau sẽ có, bây giờ nôm na hiểu rằng tính năng đó làm cho hình ảnh chạy ra giữa thay vì dạt lề trái theo mặc định. Trông cho đẹp. Nhìn code không hiểu gì vẫn biết nó làm gì là một kỹ năng quan trọng.*'
	},

	{
		name: 'Hay là viết chút tản văn',
		instruction: '**Cảnh** Bạn làm trang web Chợ Đầm để bà con ta vào đăng bán mớ tép mới xúc. Nghĩ hẳn có bà con tính đăng bán lâu dài, bạn muốn mời bà con lập tài khoản:

```html

Tên bà con:
 <input type="text">
Email:
 <input type="email">
Mật khẩu:
 <input type="password">
```

Ở đơn trên, bạn thấy là ta chỉ dùng thẻ ```<input>``` nhưng những thẻ này có tính năng ```type=""``` khác nhau. Bởi vì thông tin bà con nhập vào cho 3 dòng trên sẽ khác. 

- Tên bà con chỉ cần 1 dòng bình thường, vì thế để ```type="text"```.

- Email khác với văn bản thường ở chỗ email có cấu trúc ngữ pháp đặc biệt, ví dụ có chữ a còng ```@``` hoặc có dấu chấm ```.``` và không thể có dấu phẩy ```,```. Nếu bạn viết rõ cho trình duyệt hiểu rằng dòng này tôi muốn bà con viết email của họ vào, trình duyệt sẽ hiểu được và nếu bà con nhập ký tự không được cho phép trong email thì trình duyệt sẽ báo.  

- Password (mật khẩu) cần độ bảo mật cao, cho nên nếu ta nói rõ là đây là khung mật khẩu thì khi bà con gõ mật mã vào trình duyệt sẽ che đi bằng toàn dấu sao ```*```

**Tản văn** Sau khi bà con đăng nhập vào chợ, ngoài việc bán chuối chín cây bạn còn muốn để bà con đăng chút tản văn lơ đãng dưới vòm trời lồng lộng vang vọng tiếng sáo diều và xao xác cánh lá me bay. Bạn tạo đơn sau:

```html

Bà con muốn đăng ở:
 <select>
  <option>Sạp hàng</option>
  <option>Bảng tin</option>
 </select>

Nội dung tản văn:
 <textarea rows="10" cols="20">
 Heminway trong 500 chữ cái...
 </textarea>

Đăng chút ảnh?
 <input type="file">

<button> Đăng </button>
```

Bởi vì đơn trên sử dụng 4 thành phần mới của đơn, ta sẽ lần lượt phân tích các thành phần đó như sau:

- Phần chọn từ danh sách thả: thẻ ```<select>``` nghĩa là chọn, thẻ con ```<option>``` bên trong đó nghĩa là sự lựa chọn. Có bao nhiêu sự lựa chọn thì bạn bọc trong bấy nhiêu thẻ con option. Khi chiếu lên sẽ hiện ra thành nút chọn để bà con chọn từ danh sách thả.

- Phần điền văn bản dài: ```<textarea>``` nghĩa là khu vực viết văn bản vào. Trong ví dụ trên, độ rộng của khu vực này là 20 cột (```cols="20"```) và độ dài là 10 dòng (```rows="10"```). Nếu văn bản dài hơn thế thì sẽ tự động có cuộn để kéo xuống hoặc người dùng có thể chỉ chuột vào góc phải bên dưới khung văn bản và kéo dài rộng thêm ra.

- Phần chọn tập tin từ máy để đăng lên.

- Từ bài trước bạn đã tạo được nút bấm Đăng và Reset, giờ bạn cũng có thể tạo nút bấm bằng thẻ ```<button>```

**_Bài tập_** Trong khung làm bài tập là dàn trang web Chợ Đầm đơn giản. Tuy nhiên vẫn thiếu 2 phần đơn điền thông tin tôi nói ở trên. Bạn điền 2 đơn (1 đơn đăng ký và 1 đơn viết tản văn) vào 2 chỗ trống trong khung làm bài tập.

- Xem hắn hiển thị lên như thế nào và nghịch qua lại phần giao diện để nghĩ xem hành vi của người dùng sẽ như nào. 

(*Có thể bạn sẽ nghĩ ồ ta copy và dán y hệt code ở trên. Ta không làm vậy. Ta tự gõ vào.*)',
		body: '<header>
 <h1> Chợ Đầm </h1>
</header>

<main>
 <section>
  <h4> Bà con tính đăng bán lâu dài, mời bà con đăng ký: </h4>
  <form>


  </form>
 </section>

 <section>
  <h4> Vừa bán xong buồng chuối chín cây, hay là viết chút tản văn? </h4>
  <form>



  </form>
 </section>
</main>',
		note: ''
	},

	{
		name: 'Bố cục',
		instruction: 'Đến đây hẳn các bạn  hiểu thẻ HTML là gì và sắp xếp bố cục trang theo thẻ ```header, main, section, footer``` như thế nào. Có một thẻ chung chung hơn là thẻ ```<div> </div>``` chuyên để nhóm các thành phần lại thành nhóm để sắp xếp bố cục cho dễ. *div* là *division* nghĩa là *phần chia*, tức là chia trang thành các phần nhỏ.

**Div** Thẻ ```<div>``` giống như hộp các tông bạn mua về để đựng đồ chuyển nhà thời sinh viên, sau đó dán nhãn lên trên hộp: quần áo, xoong nồi, dụng cụ.. các kiểu. Ví dụ, bạn muốn làm ảnh thẻ kiểu polaroid, tức là có hình sau đó có bình luận ở bên dưới. Bạn có thể bọc thẻ img và thẻ p trong một thẻ div như sau:

```html

<div>
 <img src="" width="50%">
 <p> Em là ai cô gái hay nàng tiên? </p>
</div>
```
**Span** Nếu div là hộp thì thẻ ```<span></span>``` là thành phần nằm trong dòng chảy của câu bạn viết. *span* là *trải ra*. Ví dụ nếu bạn bọc chữ *này* trong thẻ span thì khi hiển thị câu của bạn không bị vỡ dòng, nếu bạn bọc trong thẻ div thì khi hiển thị chữ đó sẽ đứng ở dòng mới và bẻ câu của bạn ra. (*Xem ví dụ ở khung dưới*.)

Để cho rõ hơn, thẻ ```<div>``` rất năng động vì là thẻ chung chung bạn có thể bọc nhóm đồ vật nào cũng được (như hộp các tông chuyển nhà đó). Thẻ ```<header>``` cũng là thẻ div thôi nhưng chuyên dùng để bọc đầu trang cho nên không dùng để bọc vô tội vạ.

**Các thẻ to hơn** Từ bài học 1 đến giờ tôi mới nói đến phần thân của một văn bản HTML. Gọi là phần thân vì tất cả bọc trong thẻ thân (```<body> </body>```) và đây là phần hiển thị cho người dùng xem. Còn những thứ lặt vặt khác (máy và trình duyệt đọc) thì nằm trong phần đầu (thẻ đầu ```<head> </head>```) của văn bản HTML. Lưu ý thẻ head và thẻ header khác nhau hoàn toàn. Khung xương của một văn bản HTML căn bản như sau:

```html

<!DOCTYPE html>
<head>
 <title> </title>
 <style> </style>
<head>

<body>
 <header> </header>
 <main> </main>
 <footer> </footer>
</body>
```

Ta phân tích khung trên như sau:

- ```<!DOCTYPE html>``` là để máy biết rằng hắn đang đọc một văn bản HTML.

- ```<head>``` chứa tên trang ```<title>```, tên này hiện ra ở trên tab của trình duyệt hoặc để bộ máy tìm kiếm biết đường hiển thị tên trang.

- Thẻ ```<style>``` là phần chứa công đoạn bôi vẽ văn bản của bạn cho đẹp. Hắn ở phần đầu trang là vì khi trình duyệt đọc đến đó, những luật lệ tô màu hắn đọc được sẽ chảy tràn xuống dưới trang (vậy nên gọi là CSS: cascading stylesheet). Thường thì phần CSS cũng dài ngang văn bản HTML cho nên thường được cho sang tập tin riêng và nhập vào văn bản HTML như sau:

```html

<head>
 <link rel="stylesheet" type="text/css" href="mystyle.css">
</head>
```

*Lưu ý ở thẻ ```link``` trên, tính năng ```href``` (đường dẫn) chỉ có tên văn bản không (```mystyle.css```), điều này có nghĩa là tập tin ```mystyle.css``` ở cùng thư mục với tập tin ```.html``` của bạn.*',

		body: '<h5> Ví dụ dùng thẻ span </h5>
 <p>Sao em biết <span style="color:purple"> anh nhìn </span> mà nghiêng nón,</p>

<h5> Ví dụ dùng thẻ div </h5>
 <p>Trời mùa thu <div style="color:purple"> mây che </div> có nắng đâu..</p>',
		note: '**Bài tập thêm** Tự mở một văn bản trắng bất kỳ trên máy của bạn (bằng gedit hoặc notepad), 

- Gõ khung xương HTML vào đó

- Sáng tác thêm nội dung văn bản 

- Lưu tập tin, ví dụ: ```webcuatoi.html```.

- Mở tập tin bằng trình duyệt của bạn, trình duyệt sẽ đọc và hiển thị văn bản của bạn đẹp như bạn đã đánh dấu cho hắn.'
	},
	{
		name: 'Tô màu',
		instruction: '<script>
$(document).ready(function(){
 $(".bmark").on("click", function(){
  window.location.hash="";  
  window.location.hash= $(this).attr("href");
 });

 $(document).on("click", "#toc", function(){
  $(".toc").toggle();
 });
 
 $(document).on("click", "#khung", function(){
  $("#result").css("border","none");
  if ($(window).height() > $(window).width()) {
   $(".main-container").toggleClass("main-container-fixed-phone");
   $(".section-container").toggleClass("section-container-phone");
  } else {
   $(".main-container").toggleClass("main-container-fixed");
  };
 });
})
</script>
<style>
#toc, #khung {
 position:fixed;
 right:1%;
 border:none;
 color: ghostwhite;
 opacity: 0.9;
 border-radius: 5%;
 font-weight: bold;
}
#toc {
 top: 1%;
 background-color:#f6d57e;
 padding: 1%;
}
#khung {
 background-color: #ff654d;
 bottom: 12%;
 padding: 1% 2%;
}
.main-container-fixed{
 position:fixed;
 background-color: ghostwhite;
 top: 50%;
 height: 50%;
 width: 70%;
 overflow: hidden;
}
.main-container-fixed-phone{
 position:fixed;
 background-color: ghostwhite;
 top: 30%;
 height: 70%;
 width: 70%;
 flex-flow: column;
 overflow: hidden;
}
.section-container-phone {
 width: 100% !important;
 height: 50% !important;
}
.toc {
 display:none;
 position:fixed;
 top: 10%;
 right: 1%; 
 background-color: #f6d57e;
 opacity: 0.9;
 border-radius: 2%;
 padding: 0.5%;
}
.toc ul {
 list-style:none;
 margin:0;
 padding:0;
}
</style>
<div class="toc"> 
<ul>
<li><a href="#pre" class="bmark" onclick="return false">0. Chuẩn bị</a></li>
<li>Màu</li>
<li>Font</li>
<li><a href="#hea" class="bmark" onclick="return false">1. Đầu</a></li>
<li>Nền</li>
<li><a href="#hcol" class="bmark" onclick="return false">Màu chữ</a></li>
<li><a href="#hpad" class="bmark" onclick="return false">Đệm..</a></li>
<li><a href="#mai" class="bmark" onclick="return false">2. Chính</a></li>
<li>Góc ảnh</li>
<li>Rộng ảnh</li>
<li>Căn đoạn</li>
<li><a href="#foo" class="bmark" onclick="return false">3. Chân</a></li>
<li>Căn chân</li>
<li>Gộp CSS</li>
<li><a href="#area" class="bmark" onclick="return false"> 4. Khung </a></li>
</ul>
</div>
<button id="toc">nội dung</button>
<button id="khung">khung</button>
Ở phần HTML, ta đã xây được nội dung căn bản cho 3 trang: blog cá nhân Mưa buồn viễn xứ, trang giao dịch Chợ Đầm, và trang làm đề trắc nghiệm giảng dạy vật lý. Giờ ta sẽ dùng CSS để tô vẽ cho các trang này đẹp lên. 

**_Bài tập_** Bắt đầu với Mưa buồn viễn xứ.

- *Bởi vì đây là bài thực hành nên trang này có thêm 1 số nút, và đây là hướng dẫn sử dụng:*

- *Ấn vào nút ```nội dung``` để hiện ra danh sách nội dung trong trang này. Muốn danh sách này ẩn đi, ấn lại vào nút ```nội dung```*)

- *Ấn vào nút ```khung``` để kéo phần khung làm bài tập từ tận cuối trang lên. Muốn trả khung đó về chỗ cũ, ấn lại vào nút ```khung```*

- *Kéo khung lên sau đó cuộn phần hiển thị để xem kết quả cuối cùng. Sau đó đọc code ở khung nhập code để xem đoán được bao nhiêu nghĩa trong phần code đó.* 

- *Xóa thẻ ```<style> ... </style>``` và toàn bộ nội dung trong thẻ đó đi để làm lại từng bước cho hiểu.*

- *Khi lỗi không biết ở đâu nữa thì F5 lại trang để tải lại bài học.*

<b id="pre">0. Chuẩn bị</b>

**Bảng màu**

- Lên trang <a href="http://colorpalettes.net/" target="_blank">colorpalettes.net</a> chọn bảng màu đẹp phù hợp nội dung tâm hồn người con xa xứ. Khi bạn ấn vào bảng màu nào, hắn mở ra, bạn ấn chuột vào màu nào thì màu đó sẽ tự được copy đợi bạn dán.

- Nếu bạn không làm được bước trên thì dùng màu có sẵn của trình duyệt, màu của hắn tên thuần ví dụ ```white, black, red, green, blue``` đủ cả.

**Chọn mặt chữ**

- Lên <a href="https://fonts.google.com/" target="_blank">fonts.google.com</a> chọn mặt chữ đẹp (gõ thử vào, hoặc lọc ra các mặt chữ mà viết được tiếng việt). Sau khi chọn, *customize* mặt chữ ở trong giỏ và đọc cách nhúng vào.

- Nếu bạn chưa làm được như trên thì không cần vội, trình duyệt có sẵn mặt chữ chuẩn. Học xong về HTML, CSS căn bản (đến cuối bài này) làm theo hướng dẫn trên đó sẽ hiểu.

<b id="hea">1. Tô vẽ cho phần đầu trang</b>

**Bước 1** Mở thẻ ```<style>``` và viết vào như sau:

```css

<style>
 header {
  background-color: #d11d1d;
 }
</style>
```
Sau khi gõ vào, bạn có thể nhìn thấy sự thay đổi ở khung hiển thị, tôi giải thích đoạn code trên như sau:

- Mọi sự tô vẽ đều nằm trong thẻ ```<style>``` cho nên ta mở thẻ này và gọi thành phần ```<header>``` ra bằng cách viết header vào giữa 2 mảnh thẻ style. Các khoảng trắng không có nghĩa lý với máy tính nhưng khiến ta đọc code dễ hơn.

- Sau khi gọi header ra, ta mở đóng ngoặc kép và viết lệnh tô vẽ thành phần header này vào trong ngoặc

- Ở đây tôi viết ```background-color``` vì tôi muốn đổi màu nền của thành phần đó. *background-color* nghĩa là *màu nền*. Mặc định là màu trắng.

- Sau đó tôi viết dấu 2 chấm ```:``` và viết màu tôi muốn đổi. 

- Nếu bạn không copy được màu từ trang colorpalettes thì viết chữ ```red``` vào. Nếu copy được thì dùng ```Ctrl+V``` dán màu vừa copy    vào, ví dụ: ```#d11d1d```. Đừng hỏi đám rách việc kia là gì. Nôm na là đám đó là độ nhiều của 3 màu căn bản ```đỏ; xanh lá; xanh biển``` (cũng như bảng chữ cái, trong nỗ lực số hóa thông tin của vũ trụ này) được đánh số từ 0 đến 255 (thì phải) sau đó đổi sang hệ 16 phân viết cho gọn. 

   - ```#``` là ám hiệu bắt đầu.

   - ```d1``` là mức độ của màu đỏ, nếu bạn đổi từ 16 phân sang thập phân sẽ biết số đó là bao nhiêu, tôi không rảnh lắm.

   - ```1d``` là mức độ xanh lá 

   - ```1d``` là mức độ xanh biển

- Sau khi viết màu, tôi dùng dấu chấm phẩy ```;``` để kết thúc lệnh tô màu này. Quên dấu này sinh lỗi nghen, vì máy không quan tâm khoảng trắng, cho nên cần có biểu tượng khác để đánh dấu cuối câu, giống như khi ta viết hết câu thì chấm câu vậy.

<b id="hcol">Bước 2</b> Sau khi có màu nền, tôi thấy chữ màu đen trên nền đỏ không đẹp lắm, tôi muốn đổi sang trắng.

```css

<style>
 header {
  background-color: #d11d1d;
  color: ghostwhite;
 }
</style>
```
Bạn có thể thấy là tôi viết thêm một lệnh vào dưới lệnh tô màu nền, cấu trúc ngữ pháp tương tự như trên. ```color``` nghĩa là màu, ```ghostwhite``` là màu có sẵn của trình duyệt, tức là trắng hơi xám, bạn để ```white``` không cũng được.

<b id="hpad">Bước 3</b> Tôi thấy chữ dít vào lề quá nên muốn có chút đệm từ lề hộp vào chữ. Để làm được điều này, tôi thêm vào ```padding: 2%;```. *padding* nghĩa là *đệm*, và tôi để phần trăm cho dễ. Bạn tự thêm vào khung của mình nhé.

**Bước 4** Tôi thấy góc nhọn của hộp đầu đề sắc quá đâm vào mắt đau nên muốn làm tròn bớt các góc. Tôi thêm vào dòng sau: ```border-radius: 3%;```. *border-radius* nghĩa là *bán kính của viền*, bán kính càng to thì hộp càng tròn. Nếu bạn có ảnh đại diện hình vuông thì bạn để ```border-radius: 50%;``` sẽ thành ảnh đại diện tròn xoe rất đẹp mắt. 

**Bước 5** Tôi muốn căn chữ Mưa buồn viễn xứ ra giữa hộp cho đẹp, tôi thêm vào dòng lệnh ```text-align: center;``` *text-align* nghĩa là *căn chữ*, *center* là *giữa*. Tadaa!!!

<b id="mai">2. Phần chính</b> Bạn nhìn vào phần còn lại của trang và thấy sao?

**Bước 1** Tôi thấy các góc ảnh quá nhọn, cho nên tôi gọi ra thẻ ảnh ```<img>``` và đổi ```border-radius``` của thành phần img thành 2% như sau:

```css

<style>
 header {
  background-color: #d11d1d;
  color: ghostwhite;
  padding: 2%;
  border-radius: 3%;
  text-align: center;
 }

 img {
  border-radius: 2%;
 }
```
**Bước 2** Tôi nhận ra là cả 2 ảnh tôi đều để độ rộng là 50% hộp chứa nó, vì vậy tôi có thể cho chung ```width: 50%``` vào CSS và xóa tính năng đó ở dưới thẻ  <xmp><img src="..." width="50%" /></xmp> trong HTML đi, thành <xmp><img src="..." /></xmp>

**Bước 3** Tôi nhìn vào các thành phần ```<section>``` và thấy hắn dạt hết vào lề, tôi muốn nội dung trong các thành phần section này căn giữa. Tự thêm vào CSS dòng sau:

```css

section {
 text-align: center;
}
```
<b id="foo">3. Chân trang</b> Để đua cùng các thành phần khác, tôi cũng muốn Việt Kiều yêu nước trôi ra giữa. 

**Bước 1** Tôi có thể viết thêm:

```css

footer {
 text-align: center;
}
```
**Bước 2** Tôi nhận ra rằng tôi có 2 dòng lệnh y hệt nhau cho 2 thành phần section và footer:

```css

section {
 text-align: center;
}

footer {
 text-align: center;
}
```
Tôi có thể viết gọn lại như sau:

```css

section, footer {
  text-align: center;
}
</style>
```

<b id="area">4. Khung làm bài tập</b>',
		body: '<style>
 header {
  background-color: #d11d1d;
  color: ghostwhite;
  padding: 2%;
  border-radius: 3%;
  text-align: center;
 }

 img {
  border-radius: 2%;
  width: 50%;
 }

 section, footer {
  text-align: center;
 }
</style>

<header>
 <h1> Mưa buồn viễn xứ </h1>
 <em> ngày buồn tháng nhớ năm thương </em>
</header>

<main>
 <section> 
  <h2> Hoa vàng nhớ quê </h2>
  <img src="https://images.pexels.com/photos/1081673/pexels-photo-1081673.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" width="50%">
  <p> Nay đi làm <a href="#">nails</a> về nhớ quê day dứt. </p>
 </section>

 <section>
  <h2> Chiều biên ải </h2>
  <img src="https://images.pexels.com/photos/1366901/pexels-photo-1366901.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" width="50%">
  <p> Mênh mênh mang mang phù vân Yên Tử.. </p>
 </section>
</main>

<footer>
 <p> <a href="#">Việt Kiều yêu nước 2019</a> </p>
</footer>',
		note: ''
	},
	{
		name: 'Thiết kế năng động',
		instruction: '<script>
$(document).ready(function(){
 $(".bmark").on("click",function(){
  window.location.hash="";
  window.location.hash=$(this).attr("href");
 });

 $(document).on("click", "#toc", function(){
  $(".toc").toggle();
 });

$(document).on("click", "#khung", function(){
  $("#result").css("border","none");
  if ($(window).height() > $(window).width()) {
   $(".main-container").toggleClass("main-container-fixed-phone");
   $(".section-container").toggleClass("section-container-phone");
  } else {
   $(".main-container").toggleClass("main-container-fixed");
  };
 });
})
</script>
<style>
#toc, #khung {
 position:fixed;
 right:1%;
 border:none;
 color: ghostwhite;
 opacity: 0.9;
 border-radius: 5%;
 font-weight: bold;
}
#toc {
 top: 1%;
 background-color:#f6d57e;
 padding: 1%;
}
#khung {
 background-color: #ff654d;
 bottom: 12%;
 padding: 1% 2%;
}
.main-container-fixed{
 position:fixed;
 background-color: ghostwhite;
 top: 50%;
 height: 50%;
 width: 70%;
 overflow: hidden;
}
.main-container-fixed-phone{
 position:fixed;
 background-color: ghostwhite;
 top: 30%;
 height: 70%;
 width: 70%;
 flex-flow: column;
 overflow: hidden;
}
.section-container-phone {
 width: 100% !important;
 height: 50% !important;
}
.toc {
 display:none;
 position:fixed;
 top: 10%;
 right: 1%; 
 background-color: #f6d57e;
 opacity: 0.9;
 border-radius: 2%;
 padding: 0.5%;
}
.toc ul {
 list-style:none;
 margin:0;
 padding:0;
}
</style>

<div class="toc"> 
<ul>
<li>1. Class </li>
<li><a href="#fle" class="bmark" onclick="return false">2. Flex</a></li>
<li> 3. Flex-flow </li>
<li>4. Độ rộng </li>
<li><a href="#bor" class="bmark" onclick="return false">5. Viền</a></li>
<li>6. Lề</li>
<li>7. Góc hộp</li>
<li>8. B.tượng </li>
<li><a href="#med" class="bmark" onclick="return false">9. Media </a> </li>
<li>10. Khung</li>
</ul>
</div>
<button id="toc">nội dung</button>
<button id="khung">khung</button>

Thiết kế năng động (responsive design) là trang web của bạn khi mở trên máy tính màn hình lớn hay điện thoại màn hình bé hay máy tính bảng màn hình vừa đều không bị vỡ đẹp. Nhiều trang khi vào bằng điện thoại chữ bé tin hin không đọc được, hoặc dàn trang vỡ lổn nhổn. Như thế là không năng động. Khi thiết kế phải tính đến các yếu tố đó.

**Thẻ viewport** Luôn dán thẻ sau vào đầu trang

```html

<head>
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
```

Tôi chỉ biết viewport là độ rộng màn hình của thiết bị người dùng.

**CSS flexbox và phân loại media**

Tôi chỉ dùng flexbox, không dùng grid để thiết kế. *Flexbox* là *flexible box* nghĩa là *hộp uyển chuyển*. Khi bạn thu nhỏ cửa sổ, hay mở trang trên các thiết bị khác nhau (độ rộng của màn hình thay đổi), flexbox sẽ giúp bạn thay đổi bố cục trang rất dễ và tiện lợi, dựa theo cỡ màn hình.

**_Bài tập_** Ta tiếp tục làm trang Mưa buồn viễn xứ có khả năng phản ứng năng động khi độ rộng của màn hình thiết bị thay đổi. Trang này hiện có 3 bài đăng, mỗi bài bọc trong thẻ section.

- *Ấn nút ```nội dung``` để hiện nội dung trong trang, ấn lại để ẩn đi*

- *Ấn nút ```khung``` để kéo khung bài tập từ dưới lên, ấn lại để trả khung về chỗ*

- *Xem kết quả cuối cùng ở phần hiển thị sau đó xóa phần CSS code của hôm nay đi (hẳn bạn biết hôm trước đến đâu?)*

- *Làm lại từ đầu theo hướng dẫn*

**Nghĩ** Thí dụ tôi muốn dàn trang web Mưa buồn viễn xứ như sau: bài đăng đầu tiên là bài to cho tràn ra cả dòng. Các bài còn lại chia nhau 2 bài một dòng. Khi màn hình bé lại thì tất cả cùng tràn ra 1 bài 1 dòng.

**Bước 1** Nếu trong CSS bạn viết ```section``` tức là bạn chọn ra tất cả các thành phần section, khi đó mọi luật bạn áp dụng sẽ cho cả 3 section trong trang này.

- Muốn chọn riêng một thành phần (như ý đồ ở trên), bạn phải đặt tên cho nó, tức là bạn gán thêm tính năng class (*tập hợp*) vào thẻ của thành phần section đầu tiên trong trang: ```<section class="punch">```. Tôi gọi là *punch* giống như *punch line* tức là bài thu hút độc giả.

- Sau khi bạn đặt tên rồi thì lúc gọi thành phần này trong CSS, bạn chỉ việc thêm dấu chấm ```.``` vào trước tên đó, như này:

```html

.punch {
 ...
}
```

- Có một cách khác nữa để đặt tên thành phần, đó là tính năng id: ```<section id="punch">```, khi bạn gọi thành phần này bạn viết dấu thăng ```#``` ở trước tên thay vì dấu chấm ```.```: ```#punch```. 

**Class vs id** Khi bạn gán một tên class cho nhiều thành phần thì các thành phần đó sẽ thừa hưởng các lệnh vẽ màu CSS của class đó. Nhưng bạn chỉ được gán một id duy nhất cho một thành phần duy nhất. Thường thì class hay dùng để gọi thành phần trong CSS. Nhưng id thường để gọi thành phần trong Javascript (JS).

<b id="fle">Bước 2</b> Thêm khả năng flex cho các hộp. Muốn làm vậy ta gán ```display:flex``` cho hộp đựng các hộp con trên. Ở đây ta muốn các hộp section thay đổi năng động, cho nên ta gán dòng trên cho hộp chứa các hộp section: tức là hộp ```main```

```css

main {
 display: flex;
}
```
*display* là *hiển thị*, câu này nghĩa là hiển thị một cách uyển chuyển.

**Bước 3** Tôi muốn các hộp section chảy theo dòng hàng ngang, hết dòng thì nhảy xuống dòng mới:

```css

main {
 display: flex;
 flex-flow: row wrap;
}
```
*flex-flow* nôm na là *dòng chảy* của tính năng này, *row* là chảy theo *hàng ngang*, và *wrap* nghĩa là hết dòng thì xuống dòng mới.

**Bước 4** Với logic của các hộp như vậy, để đạt được ý đồ của ta ở trên, ta để độ rộng của hộp ```.punch``` là ```100%``` tức là chiếm cả dòng, và độ rộng của các hộp ```section``` bình thường ```50%``` tức là chiếm nửa dòng.

```css

.punch {
 width: 100%;
}
section {
 width: 50%;
}
```

<b id="bor">Bước 5</b> Bây giờ tôi muốn thử chút viền cho các hộp section này. Tôi thêm vào cho các hộp section dòng sau ```border: 1px dashed #3c3115;```. *border* nghĩa là viền, 1px là độ dầy của viền là 1 pixel, *dashed* nghĩa là viền này kẻ đứt đoạn, có các sự lựa chọn khác ví dụ như *solid* là viền liền, *dotted* là viền chấm chấm, cuối cùng cái đám lổn nhổn kia nếu bạn nhớ lại thì đó là màu, tôi đi copy trên trang colorpalettes đừng hỏi. Nếu bạn không copy được thì viết vào chữ ```green``` cũng được. Trình duyệt có sẵn nhiều màu cho bạn chọn. Bạn thấy rằng hộp ```.punch``` cũng thừa hưởng tính năng viền, bởi vì hắn cũng là ```section``` mà thôi.

**Bước 6** Hiện các viền hộp section đang sít vào nhau trông không đẹp mắt lắm. Ta muốn chúng có chút lề ```margin: 1%;```. *margin* là *lề*, lề khác với đệm (*padding*) ở chỗ lề ở ngoài viền còn đệm ở trong. Khi thêm lề 1% cho mỗi hội thì độ rộng các hộp sẽ lớn hơn 50%, vì vậy ta giảm độ rộng cơ sở xuống một chút ```width: 48%```

**Bước 7** Các góc của hộp section đang quá nhọn, hãy cho chúng tròn bớt lại ```border-radius: 2%;```

**Bước 8** Khi bạn thêm bớt đệm và lề cho các hộp, có khả năng các hộp sẽ to nhỏ khác nhau chút, vì vậy thêm dòng sau vào đầu CSS:

```css

* {
 box-sizing: border-box;
 }
```
Dấu sao ```*``` nghĩa là chọn tất cả. Cho đến bước này thì toàn bộ lệnh CSS của ta như sau:

```css

* {
 box-sizing: border-box;
 }
 main {
 display: flex;
 flex-flow: row wrap;
 }
 .punch {
  width: 100%;
 }
 section {
  width: 48%;
  border: 1px dashed #3c3115;
  margin: 1%;
  border-radius: 2%;
 }
```
**Biểu tượng** Các biểu tượng ngoài bảng chữ cái cũng được đánh số hết, bạn thấy ở cuối trang có hình trái tim, cách gọi biểu tượng đó ra là: ``` &#9829; ``` Bạn có thể xem danh sách biểu tượng ở trang <a href="https://www.w3schools.com/html/html_symbols.asp" target="_blank">w3schools.com</a>, có đủ biểu tượng Hi Lạp, toán học vv.

<b id="med">Bước 9</b> Bây giờ ta tận dụng flexbox để làm cho giao diện phản ứng năng động. Ta thêm một đoạn lệnh vào cuối để thay đổi độ rộng của các hộp section tùy theo độ rộng màn hình. Nói nôm na, ví dụ nếu độ rộng màn hình to nhất là 800px (tức là cho màn hình bé như điện thoại) thì độ rộng tất cả các hộp đổi thành 100%. Với logic là các hộp dàn hàng ngang và hết hàng thì xuống hàng mới, các hộp sẽ tự sắp xếp như hàng dọc:

```css

@media (max-width: 800px) {
 section {
  width: 100%;
 }
}
```
Bây giờ bạn thu nhỏ trình duyệt của mình lại, bạn sẽ thấy các hộp tự dàn ra thành độ rộng 100% chứ không chen chúc bé xíu 2 hộp một hàng nữa. Hoặc nếu bạn đang dùng điện thoại bạn sẽ thấy ngay hiệu ứng sau khi viết xong media query trên.

**Khung làm bài tập**',
		body: '<style>
header {
  background-color: #d11d1d;
  color: ghostwhite;
  padding: 2%;
  border-radius: 3%;
  text-align: center;
 }
 img {
  border-radius: 2%;
  width: 50%;
 }
 section, footer {
  text-align: center;
 }
 footer p {
 color: #d11d1d;
}
* {
 box-sizing: border-box;
 }
 main {
 display: flex;
 flex-flow: row wrap;
 }
 .punch {
  width: 100%;
 }
 section {
  width: 48%;
  border: 1px dashed #3c3115;
  margin: 1%;
  border-radius: 2%;
 }
@media (max-width: 800px) {
 section {
  width: 100%;
 }
</style>

<header>
 <h1> Mưa buồn viễn xứ </h1>
 <em> ngày buồn tháng nhớ năm thương </em>
</header>

<main>
 <section class="punch"> 
  <h2> Hoa vàng nhớ quê </h2>
  <img src="https://images.pexels.com/photos/1081673/pexels-photo-1081673.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" width="50%">
  <p> Nay đi làm <a href="#">nails</a> về nhớ quê day dứt. </p>
 </section>

 <section>
  <h2> Chiều biên ải </h2>
  <img src="https://images.pexels.com/photos/1366901/pexels-photo-1366901.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" width="50%">
  <p> Mênh mênh mang mang phù vân Yên Tử.. </p>
 </section>

 <section>
  <h2> Ngày tháng đó </h2>
  <img src="https://images.pexels.com/photos/1589866/pexels-photo-1589866.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"> 
  <p> Vĩnh viễn tuổi hai mươi.. </p>
 </section>
</main>

<footer>
 <p> &#9829; <a href="#">Việt Kiều yêu nước 2019</a> &#9829; </p>
</footer>',
		note: ''
	},

	{
name: 'Cận cảnh phân tử',
instruction: 'Để giới thiệu cách làm hình động trong CSS, tôi làm hình một hạt nước vương trên màn hình nhưng với độ phóng đại hàng tỉ lần.
<div class="box">
 <div class="oxygen"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen2"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen3"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen4"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen5"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen6"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen7"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen8"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen9"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen10"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
</div>

**Mắt** là do tiến hóa nhiều nghìn năm trong môi trường có các hạt ánh sáng nên tự nhiên phát triển bộ phận dò hạt ánh sáng và một khu ở não nhận đọc tín hiệu chuyển về từ bộ dò này.

- Nếu nhìn bằng mắt ta thấy một giọt nước là một giọt liền mạch.

- Phóng to vài nghìn lần hạt nước lên bằng cái phòng, hạt nước vẫn liền mạch, có điều bắt đầu thấy các chú vi khuẩn li ti quẫy đạp trong nước.

- Phóng to vài nghìn lần nữa hạt nước to như sân vận động và ta không thấy viền liền mạch nữa mà hắn bắt đầu lổn nhổn. Giống như ta thấy một đám đông lổn nhổn những đầu là đầu.

- Phóng vài trăm lần nữa ta được hình như bên dưới, tức là ta thấy các phân tử nước bơi bơi trong không gian. 

   - Mỗi phân tử có 1 nguyên tử Oxy (hạt to màu xanh biển ở giữa)

   - Và 2 nguyên tử Hydro (hạt nhỏ màu xanh lá ở bên)

   - 2 nguyên tử hydro chếch nhau khoảng 100 độ

   - Các phân tử này bơi bơi trong không gian rỗng (tức là giữa chúng không có gì cả)

   - Các phân tử này không tản ra trong phòng mà bơi bơi và hút nhau dẫn đến ta nhìn bằng mắt thường thấy một hạt

   - Các phân tử nước không hút dính vào nhau, cũng không hòa vào nhau, nếu bạn ép 2 phân tử nước lại gần nhau quá chúng đẩy nhau

   - Mỗi nguyên tử có cỡ 1 hoặc 2 x 10^-8 cm. Để gọn lại ta gọi 10^-8 cm là Angstrom.

**Nhiệt** Chuyển động quẫy đạp của các phân tử kia chính là nhiệt. Ở nhiệt độ phòng, đó là chuyển động Brown, tức là chuyển động bơi bơi của các hạt bụt ta nhìn thấy khi nắng sớm chiếu vào phòng. Đó là do các hạt bụi bị các phân tử khí bơi bơi đập vào nên cũng di chuyển bơi bơi.

- Nếu các phân tử quẫy đạp mạnh mẽ hơn (tức là nhiệt tăng, giọt nước nóng lên), ở bề mặt hạt nước, thi thoảng sẽ có phân tử quẫy mạnh quá mà thoát ra khỏi lực kéo từ bên trong của hạt nước, bay vào không gian bao la. Ta gọi là bay hơi. Chất lỏng bay hơi thì lạnh đi (do mất dần vận tốc, mất dần nhiệt).

- Khi các phân tử thoát dần thì hạt nước nhỏ lại, bay hơi hết thì cấu trúc hạt nước không còn, chỉ còn là các phân tử bay tán loạn trông không khí, cách rất xa nhau và không có ý thức gì về hạt nước mà chúng từng là một phần trong đó nữa.

**Lực** Khi mỗi phân tử nước trong không gian này đập vào tường, tường cảm thấy bị đẩy. Hàng nghìn phân tử đập vào ở những chỗ ngẫu nhiên trên toàn bộ bề mặt tường, tường sẽ chỉ cảm thấy một lực đẩy trung bình tổng thể mà thôi.

- Nếu ta không tăng độ quẫy đạp của các phân tử, mà chỉ tăng số lượng thì sao? Trước khi gấp đôi số lượng phân tử, số lượng phân tử đập vào tường trong 1 giây là x, sau khi gấp đôi, số lượng phân tử đập vào tường trong 1 giây là 2x, vì thế nhiệt tăng gấp đôi, lực tăng gấp đôi. (*Lưu ý chuyển động chính là nhiệt, là nhiệt là chuyển động, không nghĩ riêng được*).

**Ép** Nếu ta cho không khí vào một cái hộp và ép từ trên xuống, các phân tử không khí đập vào nắp sẽ đập trở lại mạnh hơn. Hắn đi va vào các phân tử khác và làm cho các phân tử khác cũng quẫy mạnh hơn. Dần dần lực ép xuống lan ra thành chuyển động nhiệt mạnh hơn của các phân tử, khối khí nóng lên và áp lực lên hộp tăng.

**_Bài tập_** Ta sẽ làm một giọt nước đơn giản: gồm 1 hộp đựng phân tử chứa 1 phân tử nước. Sau đó cho phân tử nước quẫy.

**Bước 1** Hộp chứa giọt nước cần có tính năng ```position:relative```, và giọt nước có tính năng ```position:absolute```

Ở đây hộp chứa to nhất của tôi là khung hiển thị kết quả, với ```id="result"```, vì vậy trước nhất tôi viết vào thẻ style dòng sau;

```css

<style>
#result{
position: relative;
}
</style>
```

**Bước 2** Giọt nước thực ra là một khối div, tôi sẽ đặt tên ```class="box"``` có tính năng CSS sau:

```css

.box {
	width: 210px;
	height: 210px;
        border-radius: 50%;
	border: 1px solid black;
	position: absolute;
	top: 10%;
	left: 50%;
}
```
Tôi để độ dài rộng bằng nhau  và bán kính góc 50% là để hộp div này từ hình vuông thành hình tròn xoe. Viền của khối div này tôi để 1px, viền liền, và màu đen. Bởi vì tôi để ```position:absolute``` nên ```top:10%; left:50%``` nghĩa là khối div này cách lề trái của hộp chứa nó 50% và cách lề trên của hộp chứa nó 10%.

**Bước 3** Một phân tử nước có 1 nguyên tử ô xy và 2 hi đrô.

- Ta làm ô xy trước: ô xy sẽ là một khối div khác tôi sẽ đặt tên ```class="oxygen"```, có các tính năng CSS sau:

```css

.oxygen{
	width: 20px;
	height: 20px;
	border-radius: 50%;
	position: absolute;
	top: 50%;
	left: 50%;
	background-color: blue;
	border:none;
	box-shadow: inset 0px 0px 2px 2px ghostwhite;
}
```
   - Độ dài rộng bằng nhau và bán kính góc 50% là để khối div từ vuông thành tròn xoe. 

   - Khối div này có vị trí tuyệt đối so với hộp chứa nó (*tôi sẽ cho khối này vào trong khối div giọt nước ở trên*), 50% từ trên xuống và 50% từ trái sang, tức là khối div này ở giữa giọt nước ta tạo ở trên.

   - Màu nền: xanh 

   - ```border:none;``` nghĩa là không có viền và vì nguyên tử không có cạnh sắc (*nhìn sâu vào nguyên tử sẽ lại thấy hạt nhân và mây electron bay xung quanh*) cho nên tôi sẽ làm mờ viền của nguyên tử ô xy này: ```box-shadow: inset 0px 0px 2px 2px ghostwhite;```. box-shadow nghĩa là đổ bóng, tôi cho đổ bóng cùng màu với nền trắng.

- hi đrô ta làm tương tự, nhưng nhỏ hơn, do 2 hi đrô có vị trí khác nhau nhưng những tính năng khác giống nhau, tôi làm gộp cho 2 khối div như sau:

```css

.hydrogen1, .hydrogen2 {
	width: 10px;
	height: 10px;
	border-radius: 50%;
	position: absolute;
	background-color: green;
	border:none;
	box-shadow: inset 0px 0px 1px 1px ghostwhite;
}
```
   - Sau đó mỗi khối tôi để vị trí khác nhau, thử qua thử lại để có vị trí ưng ý. Lưu ý tôi sẽ cho 2 khối này vào trong khối ô xy, vì thế vị trí của chúng là tuyệt đối so với khối ô xy chứ không so với khối giọt nước hay khung hiển thị 

```css

.hydrogen1 {
	top: 15px;
	left: 15px;
}
.hydrogen2 {
	top: 10px;
	left: -10px;
}
```
Lưu ý những tính năng CSS trên viết toàn bộ bên trong thẻ ```<style> ... </style>``` và nằm ở đầu trang để trình duyệt biết các tính năng này trước khi biết thành phần cần tô màu. Nếu hắn tải các thành phần trước, hắn sẽ không biết tô màu các thành phần đó như thế nào. Luật CSS chảy tràn từ đầu trang xuống mà.

**Bước 4** Sau đây là HTML của các khối div như đã hứa:

```html

<div class="box">
 <div class="oxygen"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
</div>
```
**Bước 5** Để các phân tử này quẫy, ta cần dùng đến Javascript. JS rất dài, tôi sẽ nói ở bài sau.

',
body: '<style>
#result{
position: relative;
}
.oxygen, .oxygen2, .oxygen3, .oxygen4, .oxygen5, .oxygen6, .oxygen7, .oxygen8, .oxygen9, .oxygen10 {
	width: 20px;
	height: 20px;
	border-radius: 50%;
	position: absolute;
	top: 50%;
	left: 50%;
	background-color: blue;
	border:none;
	box-shadow: inset 0px 0px 2px 2px ghostwhite;
}
.hydrogen1, .hydrogen2 {
	width: 10px;
	height: 10px;
	border-radius: 50%;
	position: absolute;
	background-color: green;
	border:none;
	box-shadow: inset 0px 0px 1px 1px ghostwhite;
}
.hydrogen1 {
	top: 15px;
	left: 15px;
}
.hydrogen2 {
	top: 10px;
	left: -10px;
}
.box {
	width: 210px;
	height: 210px;
        border-radius: 50%;
	border: 1px solid black;
	position: absolute;
	top: 10%;
	left: 50%;
}
</style>
<script> 
$(document).ready(function(){
function jiggleHelper(e) {
	let r = Math.round(Math.random());
	t = parseFloat(e.css("top"));
	l = parseFloat(e.css("left"));
	if (t <= 10) {
		t = 11
	}
	if (t >= 200) {
		t = 199
	}
	if (l <= 10) {
		l = 11
	}
	if (l >= 200) {
		l = 199
	}
	if (t > 10 && t < 200 && l > 10 && l < 200) {
		r2 = Math.round(Math.random());
		if (r == 0) {
			dt = Math.round(Math.random() * 100)/10;
			if (r2 == 0) {
				t += dt;
				e.css("top", t + "px");
			} else {
				t -= dt;
				e.css("top", t + "px");
			}
		} else {
			dl = Math.round(Math.random() * 100)/10;
			r2 == 0 ?
			e.css("left", (l += dl) + "px") :
			e.css("left", (l -= dl) + "px");
		}
	}

}
 
function jiggle(){
	let e = $(".oxygen");
	jiggleHelper(e);
};
function jiggle2(){
	let e = $(".oxygen2");
	jiggleHelper(e);
};
function jiggle3(){
	let e = $(".oxygen3");
	jiggleHelper(e);
};
function jiggle4(){
	let e = $(".oxygen4");
	jiggleHelper(e);
};
function jiggle5(){
	let e = $(".oxygen5");
	jiggleHelper(e);
};
function jiggle6(){
	let e = $(".oxygen6");
	jiggleHelper(e);
};
function jiggle7(){
	let e = $(".oxygen7");
	jiggleHelper(e);
};
function jiggle8(){
	let e = $(".oxygen8");
	jiggleHelper(e);
};
function jiggle9(){
	let e = $(".oxygen9");
	jiggleHelper(e);
};
function jiggle10(){
	let e = $(".oxygen10");
	jiggleHelper(e);
};

window.x = setInterval(jiggle, 100)
window.x2 = setInterval(jiggle2, 100)
window.x3 = setInterval(jiggle3, 100)
window.x4 = setInterval(jiggle4, 100)
window.x5 = setInterval(jiggle5, 100)
window.x6 = setInterval(jiggle6, 100)
window.x7 = setInterval(jiggle7, 100)
window.x8 = setInterval(jiggle8, 100)
window.x9 = setInterval(jiggle9, 100)
window.x10 = setInterval(jiggle10, 100)

 //clearInterval(x);
})
</script>

<div class="box">
 
 <div class="oxygen"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen2"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen3"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen4"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen5"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen6"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen7"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen8"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen9"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 <div class="oxygen10"><div class="hydrogen1"></div>
  <div class="hydrogen2"></div></div>
 
</div>',
note: ''
	},

	{
		name: 'Lưỡng tính sóng hạt 1',
		instruction: 'Thí nghiệm đầu tiên trong chuỗi thí nghiệm về lưỡng tính sóng hạt có sắp xếp từ trái qua như sau: 

- **Súng bắn electron** Có thể là một sợi tungsten với dòng chạy qua mạnh khiến sợi này nóng sáng. Khi các phân tử dao động điên cuồng, thi thoảng sẽ bắn ra một electron (do electron ở vòng ngoài rất xa hạt nhân)

- **Màn chắn** có 2 khe hẹp. Electron bắn ra sẽ đi về phía màn này.

- **Tường** sau màn chắn. Thi thoảng sẽ có electron qua được khe hẹp đập vào tường.

- Trên tường có **máy dò** di chuyển qua lại, dùng để đếm electron hắn bắt được.

Các bạn có thể thấy là mỗi vật trong thí nghiệm trên là một khối div không có nội dung gì ở HTML, nhưng có tính năng CSS rất khác nhau.

**Súng bắn electron**

```css

.gun {
 width: 20px;
 height: 20px;
 background-color: #879163;
 position: absolute;
 top: 50%;
}
```
- Súng này là một khối div có độ dài rộng bằng nhau và sơn màu nền xanh lá nhạt. 

- Khi ta để khung hiển thị có ```position: relative``` và hắn có ```position: absolute``` thì vị trí của hắn sẽ là tuyệt đối so với khung hiển thị.

- Vị trí của hắn từ bên trên đỉnh khung hiển thị đo xuống là ```top: 50%```

**Electron** 12 electrons là 12 khối div chung nhau những tính năng sau:

```css

.elec1, .elec2, .elec3, .elec4, .elec5 ..... {
 width: 10px;
 height: 10px;
 background-color: #d11d1d;
 border-radius: 50%;
 position: absolute;
 top: 50%;
 left: 0%;
 animation-duration: 4s;
 animation-iteration-count: infinite;
 animation-timing-function: linear;
}
```
- Ta để khối div có độ dài rộng bằng nhau và ```border-radius: 50%``` (bán kính góc 50%) là để hình vuông thành tròn xoe. 

- Vị trí ban đầu của các electron này là sát lề trái (trùng vị trí súng)

- Các electron này là hình động: độ dài thời gian của hình động là 4s, lặp vô tận và chuyển động thẳng đều

Vì mỗi electron bắn ra theo hướng khác nhau, cho nên ta làm riêng chuyển động của từng electron. Ví dụ:

```css

.elec1 {
 animation-name: shoot1;
 animation-delay: 0s;
}
```
Tên hình động của hắn là ```shoot1```. Hắn đợi 0s trước khi chạy hình động. Hình động của các electron khác được đặt tên tương tự, nhưng hoãn chạy với thời gian khác nhau. Sau đây là lệnh chạy động của electron 1:

```css

@keyframes shoot1 {
50% {
 left: 30%;
}
100% {
 top: 10%;
}
}
```

- Vì ta không nói gì, máy tính tự hiểu là ở thời điểm 0% electron này ở vị trí ban đầu.

- Ở thời điểm 50% của 4s, electron này đi tới vị trí màn chắn (```left: 30%```).

- Ở thời điểm 100% của 4s, do ta không nói gì về vị trí x của electron nên  máy tính tự hiểu là x trở về giá trị ban đầu (tức là ```left:0%```). Điều này có nghĩa là electron bật trở lại, electron này bật trở lại vị trí y cao hơn vị trí ban đầu của hắn. (```top: 10%;```)

- Do phương trình chuyển động thẳng đều máy tính tự tính ra con đường electron này đi (biết điểm đầu, giữa và cuối).

- Các electron khác tương tự. Có điều sẽ có electron qua được lỗ ở màn chắn và đi đến tường ở đằng sau (vị trí ```left: 50%```).

**Màn chắn** là 3 mảnh chia nhau một số tính chất sau:

```css

.wall1, .wall2, .wall3 {
 width: 10px;
 height: 90px;
 background-color: #3c3115;
 position: absolute;
}
```
Tức là cùng độ dài rộng, màu sơn, và cách đo vị trí. Tuy nhiên khác nhau về vị trí từ trên xuống và từ trái sang.

**Máy dò** Trên tường có một máy dò chuyển động qua lại. Electron nào bay vào đó thì hắn ghi nhận.

```css

.detect {
 width: 10px;
 height: 30px;
 background-color: blue;
 position: absolute;
 top: 25%;
 left: 48.5%;
 animation-name: move;
 animation-duration: 4s;
 animation-iteration-count: infinite;
 animation-timing-function: linear;
}
@keyframes move {
50% {
 top: 70%;
}
}
```
Các bạn có thể thấy là phần viết lệnh chạy động của máy dò chỉ có một điểm thời gian (*timeframe*) lúc 50%, bởi gì máy tính tự hiểu là điểm đầu và cuối là điểm ban đầu của khối div. Với hiểu biết rằng chuyển động của máy dò là chuyển động thẳng đều, máy tính tự cho hắn chạy.

**Tường**

```css

.back {
 width: 10px;
 height: 400px;
 background-color: #ecb573;
 position: absolute;
 top: 15%;
 left: 50%;
}
```

**Phân bố hạt electron bắn lên tường** tôi vẽ bezier curve trong thẻ canvas của HTML bằng js, không đẹp lắm dùng tạm. Cái này ai vẽ đồ họa bằng vector rồi sẽ hiểu qua.

```js

#myCanvas{
 position: absolute;
 top: 14%;
 left: 72%;
}

<canvas id="myCanvas" width="150" height="300" style="border:0px solid #d3d3d3;">
Your browser does not support the HTML5 canvas tag.</canvas>

<script>
 var c = document.getElementById("myCanvas");
 var ctx = c.getContext("2d");
 ctx.beginPath();
 ctx.moveTo(0, 0);
 ctx.bezierCurveTo(20, 80, 85, 85, 80, 150);
 ctx.stroke();

 ctx.beginPath();
 ctx.moveTo(80, 150);
 ctx.bezierCurveTo(80, 200, 20,200 , 0, 300);
 ctx.stroke()
</script>
```

**Thí nghiệm**

- Đường cong thứ nhất là phân bổ của electron qua được khe hẹp thứ nhất bắn lên tường, nếu ta che khe hẹp 2 lại.

- Rõ ràng là ở vị trí ngang hàng với khe hẹp thì tường sẽ có xác suất nhận được nhiều electron nhất. Sau đó tản ra trên dưới. Máy dò sẽ xác nhận điều này cho ta. Vì vậy đường cong thứ nhất to nhất ở vị trí ngang hàng với khe hẹp một và tản ra trên dưới.

- Ta che khe hẹp 1 lại, đường cong thứ 2 là phân bổ electron bắn đến tường qua khe hẹp 2.

**Câu hỏi** Nếu ta để mở cả 2 khe hẹp thì sự phân bổ electron bắn đến tường sẽ như nào?

**Trả lời** Xét mỗi điểm y trên tường,

- y1 là số lượng electron qua khe hẹp 1 đi tới y

- y2 là số lượng electron qua khe hẹp 2 đi tới y

- qua thời gian, số lượng electron (qua cả khe 1 và 2) tới điểm y là ```y = y1 + y2```. Ta làm phép cộng này dọc theo tường sẽ ra đường cong thứ 3. Cộng bình thường như thế gọi là cộng tuyến tính (*linearly*). Máy dò sẽ xác nhận phép cộng trên. Đây là thể hiện bản chất hạt.',
		body: '<style>
#result {
 position:relative;
 height: 500px;
}
.gun {
 width: 20px;
 height: 20px;
 background-color: #879163;
 position: absolute;
 top: 50%;
}
.elec1, .elec2, .elec3, .elec4, .elec5, .elec6, .elec7, .elec8, .elec9, .elec10, .elec11, .elec12 {
 width: 10px;
 height: 10px;
 background-color: #d11d1d;
 border-radius: 50%;
 position: absolute;
 top:50%;
 left: 0%;
 animation-duration: 4s;
 animation-iteration-count: infinite;
 animation-timing-function: linear;
}
.elec1 { animation-name: shoot1;}
.elec2 {
 animation-name: shoot2;
 animation-delay: 11s;}
.elec3{
 animation-name: shoot3;
 animation-delay: 2s;}
.elec4{
 animation-name: shoot4;
 animation-delay: 9s;}
.elec5{
 animation-name: shoot5;
 animation-delay: 4s;}
.elec6 {
 animation-name: shoot6;
 animation-delay: 7s;}
.elec7 {
 animation-name: shoot7;
 animation-delay: 6s;}
.elec8{
 animation-name: shoot8;
 animation-delay: 5s;}
.elec9{
 animation-name: shoot9;
 animation-delay: 8s;}
.elec10{
 animation-name: shoot10;
 animation-delay: 3s;}
.elec11{
 animation-name: shoot11;
 animation-delay: 1s;}
.elec12{
 animation-name: shoot12;
 animation-delay: 12s;}
@keyframes shoot1 {
50% { left: 30%;}
100% { top: 10%;}}
@keyframes shoot2 {
50% { left: 30%;}
100%{ top: 80%;}}
@keyframes shoot3 {
50% { left: 30%;}
100% { left: 50%;top: 30%;}}
@keyframes shoot4 {
50% { left: 30%;}
100%{ top: 60%;}}
@keyframes shoot5 {
50% { left: 30%;}
100%{ top: 50%;}}
@keyframes shoot6 {
50% { left: 30%;}
100% { top: 40%;}}
@keyframes shoot7 {
50% { left: 30%;}
100%{ left:50%;top: 70%;}}
@keyframes shoot8 {
50% { left: 30%;}
100% { top: 20%;}}
@keyframes shoot9 {
50% { left: 30%;}
100%{ top: 90%;}}
@keyframes shoot10 {
50% { left: 30%;}
100%{ top: 95%;}}

@keyframes shoot11{
50%{left:30%; top:40%;}
100%{left:50%;top:40%;}
}
@keyframes shoot12{
50%{left:30%; top:60%;}
100%{left:50%;top:55%;}
}

.wall1, .wall2, .wall3 {
 width: 10px;
 height: 90px;
 background-color: #3c3115;
 position: absolute;
}
.wall1 {
 top: 22%;
 left: 30%;}
.wall2 {
 top: 42%;
 left: 30%;}
.wall3 {
 top: 62%;
 left: 30%;}

.detect {
 width: 10px;
 height: 30px;
 background-color: blue;
 position: absolute;
 top: 25%;
 left: 48.5%;
 animation-name: move;
 animation-duration: 4s;
 animation-iteration-count: infinite;
 animation-timing-function: linear;
}
@keyframes move {
50% { top: 70%;}}

.back {
 width: 10px;
 height: 400px;
 background-color: #ecb573;
 position: absolute;
 top: 10%;
 left: 50%;
}

#myCanvas{
 position: absolute;
 top: 10%;
 left: 55%;
}
#myCanvas2{
 position: absolute;
 top: 35%;
 left: 55%;
}
#myCanvas3{
 position: absolute;
 top: 15%;
 left: 70%;
}
</style>

<div class="gun"></div>

<div class="elec1"></div>
<div class="elec2"></div>
<div class="elec3"></div>
<div class="elec4"></div>
<div class="elec5"></div>
<div class="elec6"></div>
<div class="elec7"></div>
<div class="elec8"></div>
<div class="elec9"></div>
<div class="elec10"></div>
<div  class="elec11"></div>
<div class="elec12"></div>

<div class="wall1"></div>
<div class="wall2"></div>
<div class="wall3"></div>

<div class="detect"></div>
<div class="back"></div>

<canvas id="myCanvas" width="150" height="300" style="border:0px solid #d3d3d3;">
Your browser does not support the HTML5 canvas tag.</canvas>

<script>
 var c = document.getElementById("myCanvas");
 var ctx = c.getContext("2d");
 ctx.beginPath();
 ctx.moveTo(0, 0);
 ctx.bezierCurveTo(20, 80, 85, 85, 80, 150);
 ctx.stroke();

 ctx.beginPath();
 ctx.moveTo(80, 150);
 ctx.bezierCurveTo(80, 200, 20,200 , 0, 300);
 ctx.stroke()
</script>
<canvas id="myCanvas2" width="150" height="300" style="border:0px solid #d3d3d3;">
 Your browser does not support the HTML5 canvas tag.</canvas>

<script>
 var c = document.getElementById("myCanvas2");
 var ctx = c.getContext("2d");
 ctx.beginPath();
 ctx.moveTo(0, 0);
 ctx.bezierCurveTo(20, 80, 85, 85, 80, 150);
 ctx.stroke();

 ctx.beginPath();
 ctx.moveTo(80, 150);
 ctx.bezierCurveTo(80, 200, 20,200 , 0, 300);
 ctx.stroke()
</script>

<canvas id="myCanvas3" width="150" height="400" style="border:0px solid #d3d3d3;">
 Your browser does not support the HTML5 canvas tag.</canvas>

<script>
 var c = document.getElementById("myCanvas3");
 var ctx = c.getContext("2d");
 ctx.beginPath();
 ctx.moveTo(0, 0);
 ctx.bezierCurveTo(50, 50, 85, 85, 80, 200);
 ctx.stroke();

 ctx.beginPath();
 ctx.moveTo(80, 200);
 ctx.bezierCurveTo(80, 300, 50,350 , 0, 400);
 ctx.stroke()
</script>',
		note: ''
	}


	])