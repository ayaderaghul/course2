class SetDefault < ActiveRecord::Migration[5.2]
  def change
  	change_column :lessons, :note, :text, default: ""
  end
end
