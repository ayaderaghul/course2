class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :content
      t.references :lesson, foreign_key: true

      t.timestamps
    end
    add_index :comments, [:lesson_id, :created_at]
  end
end
