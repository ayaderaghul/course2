class CreateLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :lessons do |t|
      t.string :name
      t.text :instruction
      t.text :body

      t.timestamps
    end
  end
end
