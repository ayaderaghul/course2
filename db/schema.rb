# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_21_202005) do

  create_table "comments", force: :cascade do |t|
    t.text "content"
    t.integer "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id", "created_at"], name: "index_comments_on_lesson_id_and_created_at"
    t.index ["lesson_id"], name: "index_comments_on_lesson_id"
  end

  create_table "lessons", force: :cascade do |t|
    t.string "name"
    t.text "instruction"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "note", default: ""
    t.integer "likes", default: 0
  end

  create_table "likes", force: :cascade do |t|
    t.integer "count", default: 0
    t.integer "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_likes_on_lesson_id"
  end

end
