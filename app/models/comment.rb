class Comment < ApplicationRecord
  belongs_to :lesson
  default_scope -> {order(created_at: :desc)}
  validates :lesson_id, presence: true
  validates :content, presence: true, length: {maximum: 140}
end
