class CommentsController < ApplicationController
	def create
		@lesson = Lesson.find(params[:comment][:lesson_id])
		@comment = @lesson.comments.build(comment_params)
		if @comment.save 
			@comments = @lesson.comments.all
			flash[:success] = 'comment added'
			respond_to do |format|
				format.html { redirect_to edit_lesson_path(@lesson)}
				format.js
			end
		else
			redirect_to edit_lesson_path(@lesson)
		end
	end
	def destroy
		@comment = Comment.find(params[:id])
		@lesson = @comment.lesson
		@comments = @lesson.comments.all 
		@comment.destroy
		flash[:success] = 'comment deleted'
		respond_to do |format|
			format.html {redirect_to edit_lesson_path(@lesson)}
			format.js
		end
	end

	
	private 
	def comment_params
		params.require(:comment).permit(:content, :lesson_id)
	end
end
