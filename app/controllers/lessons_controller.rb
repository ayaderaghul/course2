class LessonsController < ApplicationController
	def index
		@lessons = Lesson.all 
	end
	def show 
		@lesson = Lesson.find(params[:id])
	end
	def new 
		@lessons = Lesson.all
		@lesson = Lesson.new 
	end
	def create
		@lessons = Lesson.all 
		@lesson = Lesson.create(lesson_params)
	end
	def edit 
		@lessons = Lesson.all 
		@lesson = Lesson.find(params[:id])
		#@comments = @lesson.comments.all
		#@comment = @lesson.comments.build
	end
	def update
		@lessons = Lesson.all
		@lesson = Lesson.find(params[:id])
		@lesson.update_attributes(lesson_params)
		
	end
	def delete 
		@lesson = Lesson.find(params[:lesson_id])
	end
	def destroy
		@lessons = Lesson.all
		@lesson = Lesson.find(params[:id])
		@lesson.destroy 
	end
	private
	def lesson_params
		params.require(:lesson).permit(:name, :instruction, :body, :note, :likes)
	end
end
