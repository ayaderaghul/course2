$(document).ready(function(){
	$(document).on('scroll', window, function(){
		if (window.scrollY > 20) {
			$('#toTop').show();
		} else {
			$('#toTop').hide()
		}
	})
	$(document).on('click', '#toTop', function(){
		window.scrollTo(0,0);
	})
})

/*
let toggleButton = (w, buttonid) => {
    let b = document.querySelector(buttonid)
    if (w.scrollY > 20) {
	b.style.display = "block";
    } else {
	b.style.display = "none";
    }
}

document.addEventListener("DOMContentLoaded", function(){       window.onscroll = function() {
	toggleButton(window, "#toTop");	
	document.querySelector("#toTop").addEventListener("click", function() {
	    window.scrollTo(0,0);
	})
    }
})
*/
	 